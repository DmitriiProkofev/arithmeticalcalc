﻿using InformationClient.ArithmeticalCalc.IPresenters;
using InformationClient.ArithmeticalCalc.Models;
using InformationClient.ArithmeticalCalc.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationClient.ArithmeticalCalc.Presenters
{
    /// <summary>
    /// Класс представителя "Вывод".
    /// </summary>
    public class InformationClientPresenter : IInformationClientPresenter
    {
        private InformationClientView _view;
        private InformationClientModel _model;

        /// <summary>
        /// Инициализация.
        /// </summary>
        /// <param name="view">Представление.</param>
        /// <param name="model">Модель.</param>
        public void Init(InformationClientView view, InformationClientModel model)
        {
            _view = view;
            _model = model;

            _view.GetVariableValue += GetVariableValue_Handler;
            //_view.ViewClosed += ViewClosed_Handler;
        }

        private void GetVariableValue_Handler(string variableName)
        {
            _view.VariableValue = _model.GetVariableValue(variableName);
        }

        //private void ViewClosed_Handler()
        //{
        //    _view.SaveVariable -= SaveVariable_Handler;
        //    _view.ViewClosed -= ViewClosed_Handler;
        //}
    }
}
