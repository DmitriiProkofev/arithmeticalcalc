﻿using System;

namespace InformationClient.ArithmeticalCalc.IViews
{
    /// <summary>
    /// Интерфейс представления "Вывод".
    /// </summary>
    public interface IInformationClientView
    {
        /// <summary>
        /// Значение переменной.
        /// </summary>
        double? VariableValue { get; set; }

        /// <summary>
        /// Событие запроса значения переменной.
        /// </summary>
        event Action<string> GetVariableValue;
    }
}
