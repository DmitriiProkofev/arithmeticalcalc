﻿using InformationClient.ArithmeticalCalc.Models;
using InformationClient.ArithmeticalCalc.Views;

namespace InformationClient.ArithmeticalCalc.IPresenters
{
    /// <summary>
    /// Интерфейс представителя "Вывод.
    /// </summary>
    public interface IInformationClientPresenter
    {
        /// <summary>
        /// Инициализация.
        /// </summary>
        /// <param name="view">Представление.</param>
        /// <param name="Model">Модель.</param>
        void Init(InformationClientView view, InformationClientModel model);
    }
}
