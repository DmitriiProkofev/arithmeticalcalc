﻿
namespace InformationClient.ArithmeticalCalc.IModels
{
    /// <summary>
    /// Интерфейс модели "Вывод".
    /// </summary>
    public interface IInformationClientModel
    {
        /// <summary>
        /// Получение значения переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Значение переменной.</returns>
        double? GetVariableValue(string variableName);
    }
}
