﻿using InformationClient.ArithmeticalCalc.IViews;
using InformationClient.ArithmeticalCalc.Models;
using InformationClient.ArithmeticalCalc.Presenters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationClient.ArithmeticalCalc.Views
{
    /// <summary>
    /// Класс представления "Вывод".
    /// </summary>
    public class InformationClientView : IInformationClientView
    {
        #region Private Fields

        double? _variableValue;

        #endregion //Private Fields

        #region Main

        static void Main(string[] args)
        {
            //signalHandler += HandleConsoleSignal;
            //ConsoleHelper.SetSignalHandler(signalHandler, true);


            InformationClientPresenter presenter = new InformationClientPresenter();
            InformationClientModel model = new InformationClientModel();
            InformationClientView view = new InformationClientView();

            presenter.Init(view, model);

            while (true)
            {
                Console.Write("Введите имя переменной: ");
                string variableName = Console.ReadLine();

                if (variableName != "")
                {
                    if (view.GetVariableValue != null)
                        view.GetVariableValue(variableName);

                    if (view.VariableValue != null)
                        Console.WriteLine(string.Format("{0}: {1}", variableName, view.VariableValue.Value.ToString("G", CultureInfo.InvariantCulture)));
                    else
                        Console.WriteLine(string.Format("{0}: не рассчитана", variableName));
                }
                else
                {
                    Console.WriteLine("Ошибка: входная строка имела неверный формат");
                }
                Console.WriteLine();
            }
        }

        #endregion //Main

        #region IInformationClientView

        /// <summary>
        /// Значение переменной.
        /// </summary>
        public double? VariableValue
        {
            get
            {
                return _variableValue;
            }
            set
            {
                _variableValue = value;
            }
        }

        /// <summary>
        /// Событие запроса значения переменной.
        /// </summary>
        public event Action<string> GetVariableValue;

        #endregion //IInformationClientView
    }
}
