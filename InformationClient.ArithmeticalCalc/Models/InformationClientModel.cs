﻿using Core.ArithmeticalCalc.ErrorsHelper;
using InformationClient.ArithmeticalCalc.IModels;
using System;
using System.ServiceModel;

namespace InformationClient.ArithmeticalCalc.Models
{
    /// <summary>
    /// Класс модели "Вывод".
    /// </summary>
    public class InformationClientModel : IInformationClientModel
    {
        /// <summary>
        /// Получение значения переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Значение переменной.</returns>
        public double? GetVariableValue(string variableName)
        {
            double? variableValue = null;
            try
            {
                VariableService.VariableServiceClient variableServiceClient = new VariableService.VariableServiceClient();

                variableValue = variableServiceClient.GetVariableValue(variableName);
            }
            catch (FaultException<InvalidVariableNameFault>)
            {
                Console.WriteLine(string.Format("Ошибка: неизвестная переменная - {0}", variableName));
            }
            catch (FaultException<InvalidFormulaOfContentFault>)
            {
                Console.WriteLine("Ошибка: некорректная формула ");
            }
            catch
            {
                Console.WriteLine("Ошибка: обработки данных");
            }
            return variableValue;
        }
    }
}
