﻿using System.Runtime.Serialization;

namespace Core.ArithmeticalCalc.ErrorsHelper
{
    /// <summary>
    /// Ошибка - отсутствии переменных в бд.
    /// </summary>
    [DataContract]
    public class InvalidExistenceOfVariablesFault
    {
        [DataMember]
        public string CustomError;
        public InvalidExistenceOfVariablesFault()
        {
        }
        public InvalidExistenceOfVariablesFault(string error)
        {
            CustomError = error;
        }
    }
}
