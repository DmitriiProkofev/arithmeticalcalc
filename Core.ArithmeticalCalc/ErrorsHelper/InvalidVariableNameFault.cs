﻿using System.Runtime.Serialization;

namespace Core.ArithmeticalCalc.ErrorsHelper
{
    /// <summary>
    /// Ошибка - имя переменной уже существует.
    /// </summary>
    [DataContract]
    public class InvalidVariableNameFault
    {
        [DataMember]
        public string CustomError;
        public InvalidVariableNameFault()
        {
        }
        public InvalidVariableNameFault(string error)
        {
            CustomError = error;
        }
    }
}
