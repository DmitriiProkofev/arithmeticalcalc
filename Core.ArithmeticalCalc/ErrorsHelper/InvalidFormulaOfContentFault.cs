﻿using System.Runtime.Serialization;

namespace Core.ArithmeticalCalc.ErrorsHelper
{
    /// <summary>
    /// Ошибка - неверная формула.
    /// </summary>
    [DataContract]
    public class InvalidFormulaOfContentFault
    {
        [DataMember]
        public string CustomError;
        public InvalidFormulaOfContentFault()
        {
        }
        public InvalidFormulaOfContentFault(string error)
        {
            CustomError = error;
        }
    }
}
