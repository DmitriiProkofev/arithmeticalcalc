﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.ArithmeticalCalc.ErrorsHelper
{
    /// <summary>
    /// Стандартная ошибка. Плохое подключение к сервису. Ошибка при обращение к бд.
    /// </summary>
    [DataContract]
    public class BaseFault
    {
        [DataMember]
        public string CustomError;
        public BaseFault()
        {
        }
        public BaseFault(string error)
        {
            CustomError = error;
        }
    }
}
