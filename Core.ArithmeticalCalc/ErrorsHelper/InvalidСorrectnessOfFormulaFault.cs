﻿using System.Runtime.Serialization;

namespace Core.ArithmeticalCalc.ErrorsHelper
{
    /// <summary>
    /// Ошибка - неправильный текст формулы.
    /// </summary>
    [DataContract]
    public class InvalidСorrectnessOfFormulaFault
    {
        [DataMember]
        public string CustomError;
        public InvalidСorrectnessOfFormulaFault()
        {
        }
        public InvalidСorrectnessOfFormulaFault(string error)
        {
            CustomError = error;
        }
    }
}
