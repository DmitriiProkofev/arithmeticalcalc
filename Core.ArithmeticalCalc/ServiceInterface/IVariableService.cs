﻿using Core.ArithmeticalCalc.Domain;
using Core.ArithmeticalCalc.ErrorsHelper;
using System.Collections.Generic;
using System.ServiceModel;

namespace Core.ArithmeticalCalc.ServiceInterface
{
    /// <summary>
    /// Интерфейс сервиса объекта "Переменная".
    /// </summary>
    [ServiceContract]
    public interface IVariableService
    {
        /// <summary>
        /// Сохранение переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <param name="content">Формула.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidVariableNameFault))]
        void SaveVariable(string variableName, string content);

        /// <summary>
        /// Проверка на существование переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        [OperationContract]
        bool CheckedVariableName(string variableName);

        /// <summary>
        /// Получение всех не рассчитанных переменных.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Variable> GetVariablesNotCalculated();

        /// <summary>
        /// Сохранение значения переменной.
        /// </summary>
        /// <param name="variableId">Ид переменной.</param>
        /// <param name="value">Значение переменной.</param>
        /// <param name="isValid">Признак правильности формулы.</param>
        [OperationContract]
        void SaveVariableValue(int variableId, double? value, bool isValid);

        /// <summary>
        /// Получение значения переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(InvalidVariableNameFault))]
        [FaultContract(typeof(InvalidFormulaOfContentFault))]
        double? GetVariableValue(string variableName);

        /// <summary>
        /// Проверка переменных в формуле на существование.
        /// </summary>
        /// <param name="content">Формула.</param>
        /// <param name="errorVariables">Несуществующие переменные</param>
        /// <param name="variables">Переменные.</param>
        /// <returns>Признак несуществования переменных в бд.</returns>
        [OperationContract]
        bool ValidateVariablesByFormula(string content, out string errorVariables, out List<string> variables);

        /// <summary>
        /// Получение Ид переменной по её имени.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Ид переменной.</returns>
        [OperationContract]
        int? GetVariableIdByName(string variableName);

        /// <summary>
        /// Рассчет значения формулы.
        /// </summary>
        /// <param name="variableId">Ид переменной.</param>
        /// <param name="content">Формула.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidExistenceOfVariablesFault))]
        [FaultContract(typeof(InvalidСorrectnessOfFormulaFault))]
        void CalculatedFormula(int variableId, string content);

        /// <summary>
        /// Получение значений переменных.
        /// </summary>
        /// <param name="variables">Переменные.</param>
        /// <returns>Словарь переменная, значение.</returns>
        [OperationContract]
        Dictionary<string, double?> GetValueVariables(List<string> variables);

        /// <summary>
        /// Проверка рассчитана ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак рассчёта.</returns>
        [OperationContract]
        bool CheckedVariableIsCalculated(string variableName);

        /// <summary>
        /// Проверка верна ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак верности формулы.</returns>
        [OperationContract]
        bool CheckedFormulaIsValid(string variableName);
    }
}
