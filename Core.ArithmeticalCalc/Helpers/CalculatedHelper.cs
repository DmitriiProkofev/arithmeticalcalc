﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core.ArithmeticalCalc.Helpers
{
    public static class CalculatedHelper
    {
        public static IEnumerable<string> GetVariableNamesByContent(string content)
        {
            var regex = new Regex("[^a-zA-Z0-9]");
            content = regex.Replace(content, " ");
            var variables = content.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Where(x => Regex.IsMatch(x, "[a-zA-Z]"));

            return variables;
        }

        public static string ContentModification(string content, Dictionary<string,double?> valueVariables)
        {
            //if (valueVariables == null)
            //    return content;

            foreach (var valueVariable in valueVariables)
            {
                if (valueVariables.Values != null)
                {
                    double val = (double) valueVariable.Value;
                    content = Regex.Replace(content, valueVariable.Key, val.ToString("G", CultureInfo.InvariantCulture));
                 }
            }

            return content;
        }
    }
}
