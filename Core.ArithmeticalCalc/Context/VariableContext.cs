﻿using Core.ArithmeticalCalc.Domain;
using System.Data.Entity;

namespace Core.ArithmeticalCalc.Context
{
    public class VariableContext : DbContext
    {
        public VariableContext() : base("ArithmeticalCalcs")
        {

        }

        public DbSet<Variable> Variables { get; set; }
    }
}
