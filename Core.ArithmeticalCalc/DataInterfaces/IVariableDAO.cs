﻿
using Core.ArithmeticalCalc.Domain;
using System.Collections.Generic;

namespace Core.ArithmeticalCalc.DataInterfaces
{
    /// <summary>
    /// Интерфейс доступа к данным объекта "Переменная".
    /// </summary>
    public interface IVariableDAO
    {
        /// <summary>
        /// Сохранение переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <param name="content">Формула.</param>
        void SaveVariable(string variableName, string content);

        /// <summary>
        /// Проверка на существование переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        bool CheckedVariableName(string variableName);

        /// <summary>
        /// Получение всех не рассчитанных переменных.
        /// </summary>
        /// <returns></returns>
        List<Variable> GetVariablesNotCalculated();

        /// <summary>
        /// Сохранение значения переменной.
        /// </summary>
        /// <param name="variableId">Ид переменной.</param>
        /// <param name="value">Значение переменной.</param>
        /// <param name="isValid">Признак правильности формулы.</param>
        void SaveVariableValue(int variableId, double? value, bool isValid);

        /// <summary>
        /// Получение значения переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        double? GetVariableValue(string variableName);

        /// <summary>
        /// Проверка переменных в формуле на существование.
        /// </summary>
        /// <param name="content">Формула.</param>
        /// <param name="errorVariables">Несуществующие переменные.</param>
        /// <param name="variables">Переменные.</param>
        /// <returns>Признак несуществования переменных в бд.</returns>
        bool ValidateVariablesByFormula(string content, out string errorVariables, out List<string> variables);

        /// <summary>
        /// Получение Ид переменной по её имени.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Ид переменной.</returns>
        int? GetVariableIdByName(string variableName);

        /// <summary>
        /// Проверка рассчитана ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак рассчёта.</returns>
        bool CheckedVariableIsCalculated(string variableName);

        /// <summary>
        /// Проверка верна ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак верности формулы.</returns>
        bool CheckedFormulaIsValid(string variableName);
    }
}
