﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Core.ArithmeticalCalc.Domain
{
    [DataContract]
    public class Formula
    {
        #region Constructors

        public Formula()
        { }

        public Formula(string content)
        {
            Content = content;
        }

        #endregion //Constructors

        #region Data Properties

        /// <summary>
        /// Ид формулы.
        /// </summary>
        
        [DataMember]
        [Key]
        [ForeignKey("VariableOf")]
        public int VariableId { get; set; }

        /// <summary>
        /// Имя переменной.
        /// </summary>
        [DataMember]
        public string Content { get; set; }

        /// <summary>
        /// Переменная для формулы.
        /// </summary>
        [DataMember]
        public Variable VariableOf { get; set; }

        /// <summary>
        /// Признак верности формулы.
        /// </summary>
        [DataMember]
        public bool IsValid { get; set; }

        #endregion //Data Properties

        public override string ToString()
        {
            return Content;
        }
    }
}
