﻿using System.Runtime.Serialization;

namespace Core.ArithmeticalCalc.Domain
{
    /// <summary>
    /// Переменная.
    /// </summary>
    [DataContract]
    public class Variable
    {
        #region Constructors

        public Variable()
        { }

        public Variable(string name)
        {
            Name = name;
        }

        public Variable(string name, double? value)
        {
            Name = name;
            Value = value;
        }

        #endregion //Constructors

        #region Data Properties

        /// <summary>
        /// Ид переменной.
        /// </summary>
        [DataMember]
        public int VariableId { get; set; }

        /// <summary>
        /// Имя переменной.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Значение переменной.
        /// </summary>
        [DataMember]
        public double? Value { get; set; }

        /// <summary>
        /// Формула для переменной.
        /// </summary>
        [DataMember]
        public Formula Formula { get; set; }

        /// <summary>
        /// Признак расчета значения.
        /// </summary>
        [DataMember]
        public bool IsCalculated { get; set; }

        #endregion //Data Properties

        public override string ToString()
        {
            return Name;
        }
    }
}
