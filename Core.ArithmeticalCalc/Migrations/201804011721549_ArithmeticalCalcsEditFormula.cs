namespace Core.ArithmeticalCalc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArithmeticalCalcsEditFormula : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Formulae", "IsValid", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Formulae", "IsValid");
        }
    }
}
