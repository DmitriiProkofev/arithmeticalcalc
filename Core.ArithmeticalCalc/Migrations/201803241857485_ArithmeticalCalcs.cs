namespace Core.ArithmeticalCalc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArithmeticalCalcs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Variables",
                c => new
                    {
                        VariableId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.Double(),
                        IsCalculated = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VariableId);

            Sql("ALTER TABLE dbo.Variables ALTER COLUMN Name VARCHAR(255) COLLATE Latin1_General_CS_AS NOT NULL");

            CreateTable(
                "dbo.Formulae",
                c => new
                    {
                        VariableId = c.Int(nullable: false),
                        Content = c.String(),
                })
                .PrimaryKey(t => t.VariableId)
                .ForeignKey("dbo.Variables", t => t.VariableId)
                .Index(t => t.VariableId);

            Sql("ALTER TABLE dbo.Formulae ALTER COLUMN Content VARCHAR(255) COLLATE Latin1_General_CS_AS");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Formulae", "VariableId", "dbo.Variables");
            DropIndex("dbo.Formulae", new[] { "VariableId" });
            DropTable("dbo.Formulae");
            DropTable("dbo.Variables");
        }
    }
}
