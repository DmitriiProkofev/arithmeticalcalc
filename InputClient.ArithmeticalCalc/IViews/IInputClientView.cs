﻿using System;

namespace InputClient.ArithmeticalCalc.IViews
{
    /// <summary>
    /// Интерфейс представления "Ввод".
    /// </summary>
    public interface IInputClientView
    {
        /// <summary>
        /// Событие запускает сохранение переменной и формулы.
        /// </summary>
        event Action<string, string> SaveVariable;

        /// <summary>
        /// Событие закрытия приложения.
        /// </summary>
        event Action ViewClosed;
    }
}
