﻿using InputClient.ArithmeticalCalc.IPresenters;
using InputClient.ArithmeticalCalc.Models;
using InputClient.ArithmeticalCalc.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputClient.ArithmeticalCalc.Presenters
{
    /// <summary>
    /// Класс представителя "Ввод".
    /// </summary>
    public class InputClientPresenter : IInputClientPresenter
    {
        private InputClientView _view;
        private InputClientModel _model;

        /// <summary>
        /// Инициализация.
        /// </summary>
        /// <param name="view">Представление.</param>
        /// <param name="Model">Модель.</param>
        public void Init(InputClientView view, InputClientModel model)
        {
            _view = view;
            _model = model;

            _view.SaveVariable += SaveVariable_Handler;
            _view.ViewClosed += ViewClosed_Handler;
        }

        private void SaveVariable_Handler(string variableName, string content)
        {
            _model.SaveVariable(variableName, content);
        }

        private void ViewClosed_Handler()
        {
            _view.SaveVariable -= SaveVariable_Handler;
            _view.ViewClosed -= ViewClosed_Handler;
        }
    }
}
