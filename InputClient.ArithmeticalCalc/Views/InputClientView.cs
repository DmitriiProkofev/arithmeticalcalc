﻿using InputClient.ArithmeticalCalc.IViews;
using InputClient.ArithmeticalCalc.Models;
using InputClient.ArithmeticalCalc.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static InputClient.ArithmeticalCalc.Helpers.ExitHelper;

namespace InputClient.ArithmeticalCalc.Views
{
    /// <summary>
    /// Класс представления "Ввод".
    /// </summary>
    public class InputClientView : IInputClientView
    {
        //private static SignalHandler signalHandler;

        #region Main

        static void Main(string[] args)
        {
            //signalHandler += HandleConsoleSignal;
            //ConsoleHelper.SetSignalHandler(signalHandler, true);


            InputClientPresenter presenter = new InputClientPresenter();
            InputClientModel model = new InputClientModel();
            InputClientView view = new InputClientView();

            presenter.Init(view, model);

            while (true)
            {
                Console.WriteLine("Введите арифметическое выражение:");
                string[] arr_temp = Console.ReadLine().Split('=');

                if (arr_temp.Length == 2)
                {
                    if (view.SaveVariable != null)
                        view.SaveVariable(arr_temp[0], arr_temp[1]);
                }
                else
                {
                    Console.WriteLine("Ошибка: входная строка имела неверный формат");
                }
                Console.WriteLine();
                //Console.ReadLine();
            }
        }

        #endregion //Main

        //private static void HandleConsoleSignal(ConsoleSignal consoleSignal)
        //{
        //    InputClientView view = new InputClientView();

        //    if (view.ViewClosed != null)
        //        view.ViewClosed();

        //}

        #region IInputClientView

        /// <summary>
        /// Событие запускает сохранение переменной и формулы.
        /// </summary>
        public event Action<string,string> SaveVariable;

        public event Action ViewClosed;

        #endregion //IInputClientView
    }
}
