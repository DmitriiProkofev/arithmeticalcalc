﻿
namespace InputClient.ArithmeticalCalc.IModels
{
    /// <summary>
    /// Интерфейс модели "Ввод".
    /// </summary>
    public interface IInputClientModel
    {
        /// <summary>
        /// Сохранение переменной и её формулы.
        /// </summary>
        /// <param name="variableNaame">Имя переменной.</param>
        /// <param name="content">Формула.</param>
        void SaveVariable(string variableNaame, string content);
    }
}
