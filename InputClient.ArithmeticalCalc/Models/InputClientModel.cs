﻿using Core.ArithmeticalCalc.ErrorsHelper;
using InputClient.ArithmeticalCalc.IModels;
using System;
using System.ServiceModel;

namespace InputClient.ArithmeticalCalc.Models
{
    /// <summary>
    /// Класс модели "Ввод".
    /// </summary>
    public class InputClientModel : IInputClientModel
    {
        /// <summary>
        /// Сохранение переменной и её формулы.
        /// </summary>
        /// <param name="variableNaame">Имя переменной.</param>
        /// <param name="content">Формула.</param>
        public void SaveVariable(string variableName, string content)
        {
            try
            {
                VariableService.VariableServiceClient variableServiceClient = new VariableService.VariableServiceClient();

                variableServiceClient.SaveVariable(variableName, content);

                Console.WriteLine(string.Format("Переменная: {0} и формула: {1}  успешно созданы", variableName, content));

                var variableId = variableServiceClient.GetVariableIdByName(variableName);
                if (variableId!=null)
                {
                    variableServiceClient.CalculatedFormula((int)variableId, content);
                }

                Console.WriteLine(string.Format("Значение переменной: {0} рассчитано", variableName));
            }
            catch (FaultException<InvalidVariableNameFault>)
            {
                Console.WriteLine(string.Format("Ошибка: переменная - {0} уже существует", variableName));
            }
            catch 
            {
                Console.WriteLine("Ошибка: обработки данных");
            }
        }
    }
}
