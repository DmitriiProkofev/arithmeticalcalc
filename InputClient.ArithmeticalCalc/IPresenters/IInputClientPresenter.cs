﻿using InputClient.ArithmeticalCalc.Models;
using InputClient.ArithmeticalCalc.Views;

namespace InputClient.ArithmeticalCalc.IPresenters
{
    /// <summary>
    /// Интерфейс представителя "Ввод".
    /// </summary>
    public interface IInputClientPresenter
    {
        /// <summary>
        /// Инициализация.
        /// </summary>
        /// <param name="view">Представление.</param>
        /// <param name="Model">Модель.</param>
        void Init(InputClientView view, InputClientModel model);
    }
}
