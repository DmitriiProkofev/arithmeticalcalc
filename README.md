﻿# ArithmeticalCalc
Пример ArithmeticalCalc:

Приложение-калькулятор арифметических формул.

Запуск:

- В Service.ArithmeticalCalc\App.Config изменить строку подключения к базе данных (для контекста данных VariableContext)

  <connectionStrings>
    <add name="Core.ArithmeticalCalc.Context.VariableContext" connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=ArithmeticalCalcs;Integrated Security=True" providerName="System.Data.SqlClient" />
  </connectionStrings>

- С помощью консоли диспетчера пакетов выполнить миграции базы данных.

	- выбрать проект по умолчанию: Core.ArithmeticalCalc;
	- выполнить миграции: Update-Database.

- Запустить приложение.

Структура приложения

 - Core.ArithmeticalCalc: бизнес логика и миграции к бд;
 - Data.ArithmeticalCalc: доступ к данным бд;
 - Service.ArithmeticalCalc: wcf служба, доступ к Data;
 - InformationClient.ArithmeticalCalc: клиент для получения значений переменных;
 - InputClient.ArithmeticalCalc: клиент для ввода формул.

Примечания:

 - вещественные числа должны вводиться в следующем формате: 2.234;
 - данная версия программы не осуществляет расчёт формул в фоновом режиме, значение переменной рассчитывается после сохранения переменной и формулы в бд.
 - запускать лучше под Release, отключив ф-ию "только мой код", иначе во время отладки генерируются пользовательские исключения.