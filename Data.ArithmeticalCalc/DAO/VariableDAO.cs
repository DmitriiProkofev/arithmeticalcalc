﻿using Core.ArithmeticalCalc.Context;
using Core.ArithmeticalCalc.DataInterfaces;
using Core.ArithmeticalCalc.Domain;
using Core.ArithmeticalCalc.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Data.Entity;

namespace Data.ArithmeticalCalc.DAO
{
    /// <summary>
    /// Класс доступа к данным объекта "Переменная".
    /// </summary>
    public class VariableDAO : IVariableDAO
    {
        VariableContext variableContext = new VariableContext();

        #region IVariableDAO

        /// <summary>
        /// Сохранение переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <param name="content">Формула.</param>
        public void SaveVariable(string variableName, string content)
        {
            var variable = new Variable
            {
                Name = variableName,
                Value = null,
                IsCalculated = false,
            };
            if (content != null)
            {
                variable.Formula = new Formula
                {
                    Content = content
                };
            }

            // Добавить в DbSet
            variableContext.Variables.Add(variable);

            // Сохранить изменения в базе данных
            variableContext.SaveChanges();
        }

        /// <summary>
        /// Проверка на существование переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        public bool CheckedVariableName(string variableName)
        {
            return variableContext.Variables.Any(v => v.Name == variableName);
        }

        /// <summary>
        /// Получение всех не рассчитанных переменных.
        /// </summary>
        /// <returns></returns>
        public List<Variable> GetVariablesNotCalculated()
        {
            return variableContext.Variables.Where(v => v.IsCalculated == false).ToList();
        }

        /// <summary>
        /// Сохранение значения переменной.
        /// </summary>
        /// <param name="variableId">Ид переменной.</param>
        /// <param name="value">Значение переменной.</param>
        /// <param name="isValid">Признак правильности формулы.</param>
        public void SaveVariableValue(int variableId, double? value, bool isValid)
        {
            var variable = variableContext.Variables.Include(c => c.Formula).Where(v => v.VariableId == variableId).FirstOrDefault();

            if (variable != null)
            {
                variable.Value = value;
                variable.IsCalculated = true;
                variable.Formula.IsValid = isValid;
            }        

            // Сохранить изменения в базе данных
            variableContext.SaveChanges();
        }

        /// <summary>
        /// Получение значения переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        public double? GetVariableValue(string variableName)
        {
            double? value = null;
            var variable = variableContext.Variables.FirstOrDefault(v => v.Name == variableName);

            if (variable != null)
            {
                value = variable.Value;
            }

            return value;
        }

        /// <summary>
        /// Проверка переменных в формуле на существование.
        /// </summary>
        /// <param name="content">Формула.</param>
        /// <param name="errorVariables">Несуществующие переменные.</param>
        /// <param name="variables">Переменные.</param>
        /// <returns>Признак несуществования переменных в бд.</returns>
        public bool ValidateVariablesByFormula(string content, out string errorVariables, out List<string> variables)
        {
            bool success = true;
            errorVariables = "";
            var validateVariables = new List<string>();

            variables = CalculatedHelper.GetVariableNamesByContent(content).ToList();
            
            if (variables == null || !variables.Any())
                return success;

            foreach (var variable in variables)
            {
                if (!variableContext.Variables.Any(v => v.Name == variable))
                {
                    success = false;
                    errorVariables += string.Format("{0}, ", variable);
                }
                else
                {
                    validateVariables.Add(variable);
                }
            }

            errorVariables = errorVariables.TrimEnd(new[] { ' ', ',' });

            if (validateVariables != null)
                variables = validateVariables;
            else
                variables = null;

            return success;
        }

        /// <summary>
        /// Получение Ид переменной по её имени.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Ид переменной.</returns>
        public int? GetVariableIdByName(string variableName)
        {
            int? variableId = null;

            var variable = variableContext.Variables.FirstOrDefault(v =>  v.Name == variableName);

            if (variable != null)
            {
                variableId = variable.VariableId;
            }

            return variableId;
        }

        /// <summary>
        /// Проверка рассчитана ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак рассчёта.</returns>
        public bool CheckedVariableIsCalculated(string variableName)
        {
            return variableContext.Variables.Any(v => v.Name == variableName && v.IsCalculated == true);
        }

        /// <summary>
        /// Проверка верна ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак верности формулы.</returns>
        public bool CheckedFormulaIsValid(string variableName)
        {
            var variable = variableContext.Variables.Include(c => c.Formula).FirstOrDefault(v => v.Name == variableName);

            if (variable != null)
            {
                if (variable.Formula != null)
                {
                    if (variable.Formula.IsValid)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        #endregion //IVariableDAO
    }
}
