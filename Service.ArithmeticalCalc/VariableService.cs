﻿using Core.ArithmeticalCalc.Domain;
using Core.ArithmeticalCalc.ErrorsHelper;
using Core.ArithmeticalCalc.ServiceInterface;
using Data.ArithmeticalCalc.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Core.ArithmeticalCalc.Helpers;

namespace Service.ArithmeticalCalc
{
    /// <summary>
    /// Класс сервиса объекта "Переменная".
    /// </summary>
    public class VariableService : IVariableService
    {
        /// <summary>
        /// Сохранение переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <param name="content">Формула.</param>
        public void SaveVariable(string variableName, string content)
        {
            VariableDAO variableDAO = new VariableDAO();

            if (variableDAO.CheckedVariableName(variableName))
            {
                throw new FaultException<InvalidVariableNameFault>(new
                    InvalidVariableNameFault());
            }
            variableDAO.SaveVariable(variableName, content);
        }

        /// <summary>
        /// Проверка на существование переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        public bool CheckedVariableName(string variableName)
        {
            bool check;
            try
            {
                VariableDAO variableDAO = new VariableDAO();
                check = variableDAO.CheckedVariableName(variableName);
            }
            catch
            {
                check = false;
            }
            return check;
        }

        /// <summary>
        /// Получение всех не рассчитанных переменных.
        /// </summary>
        /// <returns></returns>
        public List<Variable> GetVariablesNotCalculated()
        {
            List<Variable> variables;

            VariableDAO variableDAO = new VariableDAO();
            variables = variableDAO.GetVariablesNotCalculated();

            return variables;
        }

        /// <summary>
        /// Сохранение значения переменной.
        /// </summary>
        /// <param name="variableId">Ид переменной.</param>
        /// <param name="value">Значение переменной.</param>
        /// <param name="isValid">Признак правильности формулы.</param>
        public void SaveVariableValue(int variableId, double? value, bool isValid)
        {
            VariableDAO variableDAO = new VariableDAO();
            variableDAO.SaveVariableValue(variableId, value, isValid);
        }

        /// <summary>
        /// Получение значения переменной.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns></returns>
        public double? GetVariableValue(string variableName)
        {
            double? value;

            VariableDAO variableDAO = new VariableDAO();

            if (variableDAO.CheckedVariableName(variableName))
            {
                if (!variableDAO.CheckedFormulaIsValid(variableName) && variableDAO.CheckedVariableIsCalculated(variableName))
                {
                    throw new FaultException<InvalidFormulaOfContentFault>(new
                        InvalidFormulaOfContentFault());
                }
            }
            else
            {
                throw new FaultException<InvalidVariableNameFault>(new
                    InvalidVariableNameFault());

            }

            value = variableDAO.GetVariableValue(variableName);

            return value;
        }

        /// <summary>
        /// Проверка переменных в формуле на существование.
        /// </summary>
        /// <param name="content">Формула.</param>
        /// <param name="errorVariables">Несуществующие переменные.</param>
        /// <param name="variables">Переменные.</param>
        /// <returns>Признак несуществования переменных в бд.</returns>
        public bool ValidateVariablesByFormula(string content, out string errorVariables, out List<string> valueVariables)
        {
            bool success;

            VariableDAO variableDAO = new VariableDAO();
            success = variableDAO.ValidateVariablesByFormula(content, out errorVariables, out valueVariables);

            return success;
        }

        /// <summary>
        /// Получение Ид переменной по её имени.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Ид переменной.</returns>
        public int? GetVariableIdByName(string variableName)
        {
            int? variableId;

            VariableDAO variableDAO = new VariableDAO();
            variableId = variableDAO.GetVariableIdByName(variableName);

            return variableId;
        }

        /// <summary>
        /// Рассчет значения формулы.
        /// </summary>
        /// <param name="variableId">Ид переменной.</param>
        /// <param name="content">Формула.</param>
        public void CalculatedFormula(int variableId, string content)
        {
            VariableDAO variableDAO = new VariableDAO();
            string errorVariables;
            var variables = new List<string>();

            if (variableDAO.ValidateVariablesByFormula(content, out errorVariables, out variables))
            {
                var valueVariables = GetValueVariables(variables);

                if (!valueVariables.ContainsValue(null))
                {
                    content = CalculatedHelper.ContentModification(content, valueVariables);

                    try
                    {
                        var func = (Func<double>)DynamicExpression.ParseLambda(new System.Linq.Expressions.ParameterExpression[0], typeof(double), content).Compile();
                        double result = func();

                        variableDAO.SaveVariableValue(variableId, result, true);
                    }
                    catch 
                    {
                        variableDAO.SaveVariableValue(variableId, null, false);
                    }
                }
                else
                {
                    variableDAO.SaveVariableValue(variableId, null, false);
                }
            }
            else
            {
                variableDAO.SaveVariableValue(variableId, null, false);
            }
        }

        /// <summary>
        /// Получение значений переменных.
        /// </summary>
        /// <param name="variables">Переменные.</param>
        /// <returns>Словарь переменная, значение.</returns>
        public Dictionary<string, double?> GetValueVariables(List<string> variables)
        {
            Dictionary<string, double?> valueVariables = new Dictionary<string, double?>();

            VariableDAO variableDAO = new VariableDAO();

            foreach (var variable in variables)
            {
                valueVariables.Add(variable, variableDAO.GetVariableValue(variable));
            }

            return valueVariables;
        }

        /// <summary>
        /// Проверка рассчитана ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак рассчёта.</returns>
        public bool CheckedVariableIsCalculated(string variableName)
        {
            bool check;
            try
            {
                VariableDAO variableDAO = new VariableDAO();
                check = variableDAO.CheckedVariableIsCalculated(variableName);
            }
            catch
            {
                check = false;
            }
            return check;
        }

        /// <summary>
        /// Проверка верна ли формула.
        /// </summary>
        /// <param name="variableName">Имя переменной.</param>
        /// <returns>Признак верности формулы.</returns>
        public bool CheckedFormulaIsValid(string variableName)
        {
            bool check;
            try
            {
                VariableDAO variableDAO = new VariableDAO();
                check = variableDAO.CheckedFormulaIsValid(variableName);
            }
            catch
            {
                check = false;
            }
            return check;
        }
    }
}
